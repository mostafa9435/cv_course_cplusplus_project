#include "Triangulation.hpp"
#include "tools.hpp"
#include <cstring>

using namespace CVLab;
using namespace cv;
using namespace std;

Triangulation::Triangulation(const Calibration &c) :
		calib(c) {
}

Triangulation::Triangulation(const Triangulation &other) :
		calib(other.calib) {
}

vector<Point3f> Triangulation::operator()(const vector<Point2f> &markers1,
		const vector<Point2f> &markers2) const {
	//throw "Triangulation::operator() is not implemented";

	float c2w[4][4];
	float proj_1[3][4];
	float proj_2[3][4];

	for (unsigned int i = 0; i < 4; i++) {
		for (unsigned int j = 0; j < 4; j++) {
			c2w[i][j] = calib.getTransCamera1World().at<float>(i, 0)
					* calib.getTransCamera1Camera2().at<float>(0, j)
					+ calib.getTransCamera1World().at<float>(i, 1)
							* calib.getTransCamera1Camera2().at<float>(1, j)
					+ calib.getTransCamera1World().at<float>(i, 2)
							* calib.getTransCamera1Camera2().at<float>(2, j)
					+ calib.getTransCamera1World().at<float>(i, 3)
							* calib.getTransCamera1Camera2().at<float>(3, j);

		}
	}

	for (unsigned int i = 0; i < 3; i++) {
		for (unsigned int j = 0; j < 4; j++) {
			proj_1[i][j] = calib.getCamera1().at<float>(i, 0)
					* calib.getTransCamera1World().at<float>(0, j)
					+ calib.getCamera1().at<float>(i, 1)
							* calib.getTransCamera1World().at<float>(1, j)
					+ calib.getCamera1().at<float>(i, 2)
							* calib.getTransCamera1World().at<float>(2, j);
		}
	}

	for (unsigned int i = 0; i < 3; i++) {
		for (unsigned int j = 0; j < 4; j++) {
			proj_2[i][j] = calib.getCamera2().at<float>(i, 0) * c2w[0][j]
					+ calib.getCamera2().at<float>(i, 1) * c2w[1][j]
					+ calib.getCamera2().at<float>(i, 2) * c2w[2][j];
		}
	}

	vector<Point2f> newPoints1;
	vector<Point2f> newPoints2;
	correctMatches(calib.getFundamentalMat(), markers1, markers2, newPoints1,
			newPoints2);

	vector<Point3f> tri_result;

	cv::Mat proj1(3, 4, CV_64F);
	cv::Mat proj2(3, 4, CV_64F);
	std::memcpy(proj1.data, proj_1, 3*4*sizeof(float));
	std::memcpy(proj2.data, proj_2, 3*4*sizeof(float));
	cv::Mat pnts3D(1, 2, CV_64FC4);

	triangulatePoints(proj1, proj2, newPoints1, newPoints2, pnts3D);

	Point3f first_marker;
	Point3f second_marker;

	first_marker.x = pnts3D.at<float>(0, 0) / pnts3D.at<float>(3, 0);
	second_marker.x = pnts3D.at<float>(0, 1) / pnts3D.at<float>(3, 1);

	first_marker.y = pnts3D.at<float>(1, 0) / pnts3D.at<float>(3, 0);
	second_marker.y = pnts3D.at<float>(1, 1) / pnts3D.at<float>(3, 1);

	first_marker.z = pnts3D.at<float>(2, 0) / pnts3D.at<float>(3, 0);
	second_marker.z = pnts3D.at<float>(2, 1) / pnts3D.at<float>(3, 1);

	tri_result.push_back(first_marker);
	tri_result.push_back(second_marker);

	return tri_result;
}

vector<vector<Point3f> > Triangulation::operator()(
		const vector<vector<Point2f> > &markers1,
		const vector<vector<Point2f> > &markers2) const {
// do nothing if there is no data
	if (markers1.empty()) {
		return vector<vector<Point3f>>();
	}

// check for same number of frames
	if (markers1.size() != markers2.size()) {
		throw "different number of frames";
	}

// create result vector
	vector<vector<Point3f>> result(markers1.size());

// trinagulate each frame for itself and store result
	for (unsigned int i = 0; i < markers1.size(); ++i) {
		result[i] = (*this)(markers1[i], markers2[i]);
	}

// and return result
	return result;
}

vector<vector<Point3f> > Triangulation::calculateMotion(
		const vector<vector<Point3f> > &data) {
	vector<vector<Point3f> > result;
	Point3f delta_marker_0;
	Point3f delta_marker_1;
	vector<Point3f> delta_marker;

//throw "Triangulation::calculateMotion is not implemented";
	for (unsigned i = 0; i < data.size(); i++) {
		delta_marker_0.x = data[i][0].x - data[0][0].x;
		delta_marker_1.x = data[i][1].x - data[0][1].x;

		delta_marker_0.y = data[i][0].y - data[0][0].y;
		delta_marker_1.y = data[i][1].y - data[0][1].y;

		delta_marker_0.z = data[i][0].z - data[0][0].z;
		delta_marker_1.z = data[i][1].z - data[0][1].z;

		delta_marker.clear();
		delta_marker.push_back(delta_marker_0);
		delta_marker.push_back(delta_marker_1);

		result.push_back(delta_marker);

	}
	return result;

}
