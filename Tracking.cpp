#include "Tracking.hpp"
#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/video.hpp>
#include <opencv2/opencv.hpp>

#include "tools.hpp"
#include "Constants.hpp"
#include "Calibration.hpp"
#include "Sequence.hpp"
#include "Tracking.hpp"
#include "Triangulation.hpp"
#include <string>
#include <iostream>

using namespace CVLab;
using namespace cv;
using namespace std;

Tracking::Tracking(const Calibration &c) :
		calib(c) {
}

Tracking::Tracking(const Tracking &other) :
		calib(other.calib) {
}

vector<vector<Point2f> > Tracking::operator()(const vector<Mat> &images,
		const vector<Point2f> &initMarkers) const {
	std::vector<cv::Point2f> outMarkers;
	vector<uchar> status;
	vector<float> err;
	std::vector<cv::Point2f> good_new;
	vector<vector<Point2f> > allMarkers;
	//TermCriteria criteria = TermCriteria(
		//	(TermCriteria::COUNT) + (TermCriteria::EPS), 2, 0.03);
	vector<Point2f> current_markers=initMarkers;
	good_new.push_back(initMarkers[0]);
	good_new.push_back(initMarkers[1]);
	allMarkers.push_back(good_new);
	for (unsigned int i = 0; i < images.size()-1 ; i++) {
		calcOpticalFlowPyrLK(images[i], images[i + 1], current_markers, outMarkers,
				status, err);
		current_markers=outMarkers;
		good_new.clear();
		for (unsigned int i = 0; i < initMarkers.size(); i++) {

			// Select good points
			if (status[i] == 1) {
				good_new.push_back(outMarkers[i]);
			}
		}
		allMarkers.push_back(good_new);
	}
	return allMarkers;
}
